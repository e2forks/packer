# Deploying a Gitlab Runner on AWS with Packer

This directory contains .json config files for Packer, the image building tool from Hashicorp. We use Packer to build various AWS AMIs that will later be used by the Terraform / Gruntwork modules.

This repo is part of the [56K.Cloud How To series](https://blog.56k.cloud/gitlab-runner-aws-ami/).

## Steps

* clone the 56k.cloud Packer repository:

    ```bash
    git clone https://gitlab.com/56k/packer.git
    ```


* build the Gitlab runner image using Packer:

    ```bash
    cd packer/gitlab-runner
    packer build -var 'aws_access_key=<your_access_key_id>' -var 'aws_secret_key=<your_access_key>' gitlab-runner.json
    ```

Login the AWS Console and check the [EC2 -> AMIs](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Images:sort=name) section to confirm that the new Gitlab runner AMI is there. Make a note of the **AMI ID**.

* launch a new EC2 instance using the **AMI ID** found in the previous step. Call the following script from the `User data` section of **EC2's instance launch options** to register the runner with gitlab.com:

    ```bash
    /home/ubuntu/gitlab-runner-register.sh <environment (e.g.: dev)> <gitlab_cicd_token>
    ```

* going back to the **repository's settings -> CI / CD -> Runners -> Specific Runners**, this new runner should be available in the list. Click on `Enable for this project` to make the runner usable by the repository.


## Next steps

At this point, the Gitlab runner is attached to the project. When defining the Gitlab CI / CD pipeline in `.gitlab-ci.yml` each job will have to use a `tag` to run that job on the new Gitlab runner. This `tag` matches the environment passed to the `gitlab-runner-register.sh` script above. Example:

   ```yml
   job:
   script:
    - echo "Hello world!"
    tags:
    - <environment (e.g. dev)>
 ```



# Find out more about 56K.Cloud

We love Cloud, Containers, DevOps, and Infrastructure as Code. If you are interested in chatting connect with us on [Twitter](https://twitter.com/56kCloud) or drop us an email: [info@56K.Cloud](mailto:info@56K.Cloud]) We hope you found this article helpful. If there is anything you would like to contribute or you have questions, please let us know!